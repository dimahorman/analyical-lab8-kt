package com.horman.springlab8.config

import com.amazonaws.regions.Regions
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper
import com.horman.springlab8.util.Secrets
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration


@Configuration
class DynamoDbConfig {
    @Bean
    fun dynamoDbMapper(): DynamoDBMapper {
        Secrets.get("AWS_ACCESS_KEY_ID") ?: throw RuntimeException("AWS_ACCESS_KEY_ID is not defined")
        Secrets.get("AWS_SECRET_ACCESS_KEY") ?: throw RuntimeException("AWS_SECRET_ACCESS_KEY is not defined")
        val client = AmazonDynamoDBClientBuilder.standard().withRegion(Regions.EU_WEST_3)
            .build()
        return DynamoDBMapper(client)
    }
}
