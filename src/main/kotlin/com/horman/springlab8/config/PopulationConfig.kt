package com.horman.springlab8.config

import com.horman.springlab8.util.Secrets
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class PopulationConfig {
    companion object {
        const val DEFAULT_COUNTRY_QUANTITY = 5
        const val DEFAULT_CITY_QUANTITY = 5
        const val DEFAULT_WAREHOUSE_QUANTITY = 10
        const val DEFAULT_WORKER_QUANTITY = 20
    }

    data class PopulationOptions(
        val cityQuantity: Int,
        val warehouseQuantity: Int,
        val workerQuantity: Int,
        val countryQuantity: Int
    )

    @Bean
    fun populationOptions(): PopulationOptions {
        val cityQuantity = Secrets.get("CITY_QUANTITY")?.toInt() ?: DEFAULT_CITY_QUANTITY
        val warehouseQuantity = Secrets.get("WAREHOUSE_QUANTITY")?.toInt() ?: DEFAULT_WAREHOUSE_QUANTITY
        val workerQuantity = Secrets.get("WORKER_QUANTITY")?.toInt() ?: DEFAULT_WORKER_QUANTITY
        val countryQuantity = Secrets.get("COUNTRY_QUANTITY")?.toInt() ?: DEFAULT_COUNTRY_QUANTITY
        return PopulationOptions(cityQuantity, warehouseQuantity, workerQuantity, countryQuantity)
    }
}
