package com.horman.springlab8.api

import com.horman.cdk.data.AmazonWorker
import com.horman.springlab8.service.AmazonWorkerService
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api")
class AmazonWorkerApi(private val amazonWorkerService: AmazonWorkerService) {

    @GetMapping("/warehouses/{warehouseId}/workers")
    fun getWorkerByWarehouseId(@PathVariable warehouseId: String): List<AmazonWorker>? {
        return amazonWorkerService.getWorkersByWarehouseId(warehouseId)
    }

    @GetMapping("/cities/{city}/workers")
    fun getWorkerByCity(@PathVariable city: String): List<AmazonWorker>? {
        return amazonWorkerService.getWorkersByCity(city)
    }

    @GetMapping("/positions/{position}/workers")
    fun getWorkersByPosition(@PathVariable position: String): List<AmazonWorker>? {
        return amazonWorkerService.getWorkersByPosition(position)
    }

    @GetMapping("/workers")
    fun getWorkerById(@RequestParam hashKey: String, @RequestParam rangeKey: String): AmazonWorker? {
        return amazonWorkerService.getWorkerById(hashKey, rangeKey)
    }

    @PostMapping("/workers/populate")
    fun populateWorkers() {
        amazonWorkerService.populateWorkers()
    }
}
