package com.horman.springlab8

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class SpringLab8Application

fun main(args: Array<String>) {
    runApplication<SpringLab8Application>(*args)
}
