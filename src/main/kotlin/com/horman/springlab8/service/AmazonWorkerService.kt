package com.horman.springlab8.service

import com.amazonaws.services.dynamodbv2.datamodeling.PaginatedQueryList
import com.horman.cdk.data.AmazonWorker
import com.horman.springlab8.config.PopulationConfig
import com.horman.springlab8.dao.AmazonWorkerDao
import com.horman.springlab8.util.RandomDateGenerator
import com.horman.springlab8.util.WordGenerator
import org.springframework.stereotype.Service

@Service
class AmazonWorkerService(
    private val amazonWorkerDao: AmazonWorkerDao,
    populationOptions: PopulationConfig.PopulationOptions
) {
    private val populationService = PopulationService(populationOptions, amazonWorkerDao)

    fun getWorkersByWarehouseId(warehouseId: String): PaginatedQueryList<AmazonWorker>? {
        return amazonWorkerDao.getByWarehouseId(warehouseId)
    }

    fun getWorkersByCity(city: String): PaginatedQueryList<AmazonWorker>? {
        return amazonWorkerDao.getByCity(city)
    }

    fun getWorkersByPosition(position: String): PaginatedQueryList<AmazonWorker>? {
        return amazonWorkerDao.getByPosition(position)
    }

    fun getWorkerById(hashKey: String, rangeKey: String): AmazonWorker? {
        return amazonWorkerDao.getWorkerByKey(hashKey, rangeKey)
    }

    fun populateWorkers() {
        populationService.populate()
    }

    private class PopulationService(
        private val populationOptions: PopulationConfig.PopulationOptions,
        private val amazonWorkerDao: AmazonWorkerDao
    ) {
        companion object {
            const val DEFAULT_ADDRESS = "Hello1"
            const val COUNTRY_LENGTH = 3
            const val CITY_LENGTH = 6
        }

        fun populate() {
            for (i in (1..populationOptions.countryQuantity)) {
                populateCountry(WordGenerator.generate(COUNTRY_LENGTH))
            }
        }

        private fun populateCountry(country: String) {
            for (i in (1..populationOptions.cityQuantity)) {
                val city = WordGenerator.generate(CITY_LENGTH)
                val workers = populateCity(country, city).toList()
                amazonWorkerDao.insertWorkers(workers)
            }
        }

        private fun populateCity(country: String, city: String) = sequence {
            for (i in (1..populationOptions.countryQuantity)) {
                val warehouseId = WordGenerator.generate()
                for (worker in getWorkers(country, city, warehouseId)) {
                    yield(worker)
                }
            }
        }

        private fun getWorkers(country: String, city: String, warehouseId: String) = sequence {
            for(i in (1..populationOptions.workerQuantity)) {
                val firstName = WordGenerator.generate()
                val lastName = WordGenerator.generate()
                val dateOfBirth = RandomDateGenerator.getRandomDate()
                val address = DEFAULT_ADDRESS
                val position = WordGenerator.generate()

                yield(
                    AmazonWorker(
                        countryCode = country,
                        city = city,
                        warehouseId = warehouseId,
                        firstName = firstName,
                        familyName = lastName,
                        dateOfBirth = dateOfBirth,
                        address = address,
                        position = position
                    )
                )
            }
        }
    }
}
