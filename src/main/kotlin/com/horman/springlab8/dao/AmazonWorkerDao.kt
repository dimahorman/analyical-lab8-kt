package com.horman.springlab8.dao

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBQueryExpression
import com.amazonaws.services.dynamodbv2.datamodeling.PaginatedQueryList
import com.amazonaws.services.dynamodbv2.model.AttributeValue
import com.horman.cdk.data.AmazonWorker
import com.horman.cdk.data.AmazonWorkerIndex
import com.horman.cdk.data.AmazonWorkerTable
import org.springframework.stereotype.Repository

@Repository
class AmazonWorkerDao(private val dynamoDbMapper: DynamoDBMapper) {
    fun getByPosition(position: String): PaginatedQueryList<AmazonWorker>? {
        return queryByParameter(
            Pair(AmazonWorkerTable.POSITION, position),
            AmazonWorkerIndex.POSITION_WORKER_ID
        )
    }

    fun getByCity(city: String): PaginatedQueryList<AmazonWorker>? {
        return queryByParameter(
            Pair(AmazonWorkerTable.CITY, city),
            AmazonWorkerIndex.CITY_WORKER_ID
        )
    }

    fun getByWarehouseId(warehouseId: String): PaginatedQueryList<AmazonWorker>? {
        return queryByParameter(
            Pair(AmazonWorkerTable.WAREHOUSE_ID, warehouseId),
            AmazonWorkerIndex.WAREHOUSE_ID_WORKER_ID
        )
    }

    private fun queryByParameter(
        parameter: Pair<String, String>, // first - param name, second - param value
        index: AmazonWorkerIndex
    ): PaginatedQueryList<AmazonWorker>? {
        val mutableMapOfNames = mutableMapOf<String, String>()
        val mutableMapOfValues = mutableMapOf<String, AttributeValue>()

        mutableMapOfNames["#${parameter.first}"] = parameter.first
        mutableMapOfValues[":${parameter.first}Value"] = AttributeValue().withS(parameter.second)

        return dynamoDbMapper.query(
            AmazonWorker::class.java,
            DynamoDBQueryExpression<AmazonWorker?>()
                .withIndexName(index.getIndexName())
                .withKeyConditionExpression("#${parameter.first} = :${parameter.first}Value")
                .withExpressionAttributeNames(mutableMapOfNames)
                .withExpressionAttributeValues(mutableMapOfValues)
                .withConsistentRead(false)
        )
    }

    fun getWorkerByKey(hashKey: String, rangeKey: String): AmazonWorker? {
        return dynamoDbMapper.load(AmazonWorker::class.java, hashKey, rangeKey)
    }

    fun insertWorkers(workers: List<AmazonWorker>) {
        dynamoDbMapper.batchSave(workers)
    }

    fun insertWorker(worker: AmazonWorker) {
        dynamoDbMapper.save(worker)
    }
}
