package com.horman.springlab8.util

object RandomDateGenerator {
    fun getRandomDate(): String {
        return "${getRandomYear()}.${getRandomMonth()}.${getRandomDay()}"
    }

    private fun getRandomDay(): String {
        return transformToDateFormat((1..28).random())
    }

    private fun getRandomMonth(): String {
        return transformToDateFormat((1..12).random())
    }

    private fun getRandomYear(): String {
        return transformToDateFormat((1900..2000).random())
    }

    private fun transformToDateFormat(dayOrMonth: Int): String {
        var dateStr = dayOrMonth.toString()

        if (dateStr.length == 1) {
            dateStr = "0$dateStr"
        }

        return dateStr
    }

}
