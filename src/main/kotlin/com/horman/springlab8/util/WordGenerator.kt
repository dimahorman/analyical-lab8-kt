package com.horman.springlab8.util

object WordGenerator {
    private const val LEFT_LIMIT = 97
    private const val RIGHT_LIMIT = 122
    private const val DEFAULT_TARGET_LENGTH = 10

    fun generate(targetLength: Int): String {
        val range = (LEFT_LIMIT..RIGHT_LIMIT)
        val charSeq = sequence<Char> {
            for (i in (0 until targetLength)) {
                val value = range.random()
                yield(value.toChar())
            }
        }
        return charSeq.joinToString("") { it.toString() }
    }

    fun generate(): String {
        return generate(DEFAULT_TARGET_LENGTH)
    }
}
