package com.horman.cdk.data

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBRangeKey
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable
import org.springframework.security.crypto.keygen.Base64StringKeyGenerator

@DynamoDBTable(tableName = AmazonWorkerTable.TABLE_NAME)
data class AmazonWorker(

    @DynamoDBHashKey(attributeName = AmazonWorkerTable.PARTITION_KEY)
    var partitionKey: String? = null,

    @DynamoDBRangeKey(attributeName = AmazonWorkerTable.WORKER_ID)
    var workerId: String? = null,

    @DynamoDBAttribute(attributeName = AmazonWorkerTable.POSITION)
    var position: String? = null,

    @DynamoDBAttribute(attributeName = AmazonWorkerTable.ADDRESS)
    var address: String? = null,

    @DynamoDBAttribute(attributeName = AmazonWorkerTable.DATE_OF_BIRTH)
    var dateOfBirth: String? = null,

    @DynamoDBAttribute(attributeName = AmazonWorkerTable.FAMILY_NAME)
    var familyName: String? = null,

    @DynamoDBAttribute(attributeName = AmazonWorkerTable.FIRST_NAME)
    var firstName: String? = null,

    @DynamoDBAttribute(attributeName = AmazonWorkerTable.WAREHOUSE_ID)
    var warehouseId: String? = null,

    @DynamoDBAttribute(attributeName = AmazonWorkerTable.CITY)
    var city: String? = null,

    @DynamoDBAttribute(attributeName = AmazonWorkerTable.COUNTRY_CODE)
    var countryCode: String? = null,
) {

    init {
        workerId = workerId ?: Base64StringKeyGenerator().generateKey()
        partitionKey = partitionKey ?: "${countryCode}_${city}_${countryCode}"
    }
}

