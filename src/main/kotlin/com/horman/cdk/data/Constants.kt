package com.horman.cdk.data

object AmazonWorkerTable {
        const val TABLE_NAME = "AmazonWorkers"
        const val COUNTRY_CODE = "CountryCode"
        const val CITY = "City"
        const val WAREHOUSE_ID = "WarehouseId"
        const val FIRST_NAME = "FirstName"
        const val FAMILY_NAME = "FamilyName"
        const val DATE_OF_BIRTH = "DateOfBirth"
        const val ADDRESS = "Address"
        const val POSITION = "Position"
        const val WORKER_ID = "WorkerId"
        const val PARTITION_KEY = "Country_City_WarehouseId"
}

enum class AmazonWorkerIndex(val partitionKey: String, val sortKey: String) {
    POSITION_WORKER_ID(AmazonWorkerTable.POSITION, AmazonWorkerTable.WORKER_ID),
    CITY_WORKER_ID(AmazonWorkerTable.CITY, AmazonWorkerTable.WORKER_ID),
    WAREHOUSE_ID_WORKER_ID(AmazonWorkerTable.WAREHOUSE_ID, AmazonWorkerTable.WORKER_ID);

    fun getIndexName(): String {
        return "${partitionKey}_${sortKey}"
    }
}
