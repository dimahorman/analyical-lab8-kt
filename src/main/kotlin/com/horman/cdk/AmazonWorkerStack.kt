package com.horman.cdk

import com.horman.cdk.data.AmazonWorkerIndex
import com.horman.cdk.data.AmazonWorkerTable
import software.amazon.awscdk.core.Construct
import software.amazon.awscdk.core.RemovalPolicy
import software.amazon.awscdk.core.Stack
import software.amazon.awscdk.core.StackProps
import software.amazon.awscdk.services.dynamodb.*

class AmazonWorkerStack(scope: Construct, id: String, props: StackProps?) : Stack(scope, id, props) {
    init {
        val tableProps = TableProps.builder()
            .tableName(AmazonWorkerTable.TABLE_NAME)
            .partitionKey(
                Attribute.builder()
                    .name(AmazonWorkerTable.PARTITION_KEY)
                    .type(AttributeType.STRING)
                    .build()
            )
            .sortKey(
                Attribute.builder()
                    .name(AmazonWorkerTable.WORKER_ID)
                    .type(AttributeType.STRING)
                    .build()
            )
            .billingMode(BillingMode.PAY_PER_REQUEST)
            .removalPolicy(RemovalPolicy.DESTROY)
            .build()

        val table = Table(this, AmazonWorkerTable.TABLE_NAME, tableProps)
        addGlobalSecondaryIndex(table, AmazonWorkerIndex.CITY_WORKER_ID)
        addGlobalSecondaryIndex(table, AmazonWorkerIndex.POSITION_WORKER_ID)
        addGlobalSecondaryIndex(table, AmazonWorkerIndex.WAREHOUSE_ID_WORKER_ID)
    }

    companion object {
        private fun addGlobalSecondaryIndex(table: Table, index: AmazonWorkerIndex) {
            table.addGlobalSecondaryIndex(
                GlobalSecondaryIndexProps.builder()
                    .partitionKey(
                        Attribute.builder()
                            .name(index.partitionKey)
                            .type(AttributeType.STRING)
                            .build()
                    )
                    .sortKey(
                        Attribute.builder()
                            .name(index.sortKey)
                            .type(AttributeType.STRING)
                            .build()
                    )
                    .indexName(index.getIndexName()).build()
            )
        }
    }
}
